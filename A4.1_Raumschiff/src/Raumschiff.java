
public class Raumschiff {

	//Attribute
	private int photonentorpedoanzahl;
	private int energieversorgunginprozent;
	private int schildeinprozent;
	private int huelleinprozent;
	private int lebenserhaltungssystemeinprozent;
	private int androidenanzahl;
	private String schiffsname;
	private String[] broadcastKommunikator;
	private Ladung[] ladungsverzeichnis;
	
	//Konstruktor
	public Raumschiff(int photonentorpedoanzahl, int energieversorgunginprozent, int schildeinprozent,
			int huelleinprozent, int lebenserhaltungssystemeinprozent, int androidenanzahl, String schiffsname,
			String[] broadcastKommunikator, Ladung[] ladungsverzeichnis) {
		
		this.photonentorpedoanzahl = photonentorpedoanzahl;
		this.energieversorgunginprozent = energieversorgunginprozent;
		this.schildeinprozent = schildeinprozent;
		this.huelleinprozent = huelleinprozent;
		this.lebenserhaltungssystemeinprozent = lebenserhaltungssystemeinprozent;
		this.androidenanzahl = androidenanzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	
	//Methoden
	public int getPhotonentorpedoanzahl() {
		return photonentorpedoanzahl;
	}

	public void setPhotonentorpedoanzahl(int photonentorpedoanzahl) {
		this.photonentorpedoanzahl = photonentorpedoanzahl;
	}

	public int getEnergieversorgunginprozent() {
		return energieversorgunginprozent;
	}

	public void setEnergieversorgunginprozent(int energieversorgunginprozent) {
		this.energieversorgunginprozent = energieversorgunginprozent;
	}

	public int getSchildeinprozent() {
		return schildeinprozent;
	}

	public void setSchildeinprozent(int schildeinprozent) {
		this.schildeinprozent = schildeinprozent;
	}

	public int getHuelleinprozent() {
		return huelleinprozent;
	}

	public void setHuelleinprozent(int huelleinprozent) {
		this.huelleinprozent = huelleinprozent;
	}

	public int getLebenserhaltungssystemeinprozent() {
		return lebenserhaltungssystemeinprozent;
	}

	public void setLebenserhaltungssystemeinprozent(int lebenserhaltungssystemeinprozent) {
		this.lebenserhaltungssystemeinprozent = lebenserhaltungssystemeinprozent;
	}

	public int getAndroidenanzahl() {
		return androidenanzahl;
	}

	public void setAndroidenanzahl(int androidenanzahl) {
		this.androidenanzahl = androidenanzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String[] getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(String[] broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public Ladung[] getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(Ladung[] ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	

}
