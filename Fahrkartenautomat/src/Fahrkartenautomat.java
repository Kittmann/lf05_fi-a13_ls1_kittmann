﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	while (true) {
	    	double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	    	double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	    	fahrkartenAusgeben();
	    	rueckgeldAusgeben(rückgabebetrag);
	
	    	System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n"+
	                          "--------------------------------------------------\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
    /* Aufgabe 1: Welche Vorteile hat man durch diesen Schritt? 
       -Man kann neue Tickets einfacher einfügen und das Programm ist übersichtlicher geschrieben
     
       Aufgabe 3: Vergleichen Sie die neue Implementierung mit der alten und erläutern Sie die Vor- und Nachteile der jeweiligen Implementierung.
       -Vorteile: Die neue Implementierung bietet kürzeren Quellcode und dadurch Übersichtlichkeit außerdem muss bei der Switch-Case Funktion jeder Wert alles durchlaufen und der Compiler braucht mehr Zeit um das gewünschte Ergebnis auszugeben
     */
    	String[] fahrkartennamen = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC", "Einhorn_Zauberkarte"};
    	double[] fahrkartenpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90, 5.0};
    	
    	for (int i = 0; i < fahrkartennamen.length; i++) {
    		System.out.print(i + " " + fahrkartennamen[i]);
    		System.out.printf(": %.2f €", fahrkartenpreise[i]);
    		System.out.println();
    		
    	}
    	
    	Scanner tastatur = new Scanner(System.in);

        double ticketEinzelpreis = 1.0;
        boolean running = false;

        do {
        	System.out.print("Bitte wählen Sie eine der Ticketarten:");
        	
            int auswahl = tastatur.nextInt();
        	if (auswahl < 0 || auswahl > fahrkartennamen.length - 1) {
        		running = true;
        		System.out.println("Diese Zahl ist nicht verfügbar, Dulli :)");
        	}
        	else {
        		running = false;
                ticketEinzelpreis = fahrkartenpreise[auswahl];
                }
        	  
        } while (running);
        
        System.out.print("Wie viele Tickets werden gekauft?: ");
        double anzahlTickets = tastatur.nextInt();
    	
    	return ticketEinzelpreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMünze;
        
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),"  ");
     	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void warte (int time) {
    	try {
  			Thread.sleep(time);
  		} catch (InterruptedException e) {
  			e.printStackTrace();
  		}
    }
    
    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrscheine werden ausgegeben.");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    public static void muenzeAusgeben(double rückgabebetrag, String einheit) {
    	while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 " + einheit);
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 " + einheit);
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("0.50 " + einheit);
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("0.20 " + einheit);
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("0.10 " + einheit);
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("0.05 " + einheit);
	          rückgabebetrag -= 0.05;
        }
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO", rückgabebetrag);
     	   System.out.println("\nwird in folgenden Münzen ausgezahlt:");
     	   muenzeAusgeben(rückgabebetrag, " EURO");
        }
    }
}

/* Zu 5. Double habe ich bei dem Ticketpreis gewählt, da dieser in meinem 
Beispiel eine Kommazahl ist und dadurch lediglich die Gleitkommazahlentypen 
zur Auswahl standen, zudem ist Double genauer als Float, jedoch hätte ich auch 
Float benutzen können.

Bei Anzahl der Tickets habe ich Int genommen, weil dieser Datentyp für Ganzzahlen 
bestimmt ist und zudem groß genug, um für mein Bespiel verwendet zu werden*/

/* Zu 6. Bei der Berechnung "zuZahlenderBetrag = ticketpreis * anzahlDerTickets;" 
berechnet der Compiler die jeweiligen Bestimmungen des Ticketpreises also mit 
dem Datentyp "Double" und von Anzahl der Tickets mit dem Datentyp "Int" und 
multipliziert diese. Das Produkt der Rechnung wird in einem Double-Wert gespeichert.  */

